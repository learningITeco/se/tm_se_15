package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.AbstracEntity;
import ru.potapov.tm.repository.IRepository;
import ru.potapov.tm.repository.ProjectRepository;
import ru.potapov.tm.util.JavaToMysql;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.New;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstracEntity> implements Serializable {
    @Inject @Nullable private ServiceLocator serviceLocator;
    @NotNull private JavaToMysql javaToMysql = new JavaToMysql();
    @Nullable private String className = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.")[((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.").length-1];

    public AbstractService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;

    }
}
