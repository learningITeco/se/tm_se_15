package ru.potapov.tm.api;


import ru.potapov.tm.dto.Session;

public interface ISessionRepository<T extends IEntity> extends IRepository<Session> {
}
