package ru.potapov.tm.util;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequestScoped
public class RequestScopedBean {
    @PersistenceContext
    @Inject
    @ru.potapov.tm.annotation.EntityManager
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
