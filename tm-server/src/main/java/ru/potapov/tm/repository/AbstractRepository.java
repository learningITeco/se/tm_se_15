package ru.potapov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.persistence.EntityManager;

@Getter
@Setter
@NoArgsConstructor
public class AbstractRepository {
    @Inject
    @ru.potapov.tm.annotation.EntityManager
    @NotNull EntityManager entityManager;
}
