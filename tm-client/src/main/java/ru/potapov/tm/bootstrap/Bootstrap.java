package ru.potapov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.potapov.tm.api.*;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.command.terminal.TaskReadCommandAbstract;
import ru.potapov.tm.endpoint.Exception_Exception;
import ru.potapov.tm.endpoint.Session;
import ru.potapov.tm.endpoint.User;
import ru.potapov.tm.service.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ApplicationScoped
public class  Bootstrap implements ServiceLocator, Serializable {
    @Inject @Nullable private ISessionService sessionService;
    @Inject @Nullable private IUserService userService;
    @Inject @Nullable private IProjectService projectService;
    @Inject @Nullable private ITaskService taskService;
    @Inject @Nullable private ITerminalService terminalService;
    @NotNull private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.potapov.tm").getSubTypesOf(AbstractCommand.class);

    @NotNull final SimpleDateFormat ft            = new SimpleDateFormat("dd-MM-yyyy");

    public void init() throws Exception{
        System.out.println(projectService);
        if (classes.contains(TaskReadCommandAbstract.class))
            classes.remove(TaskReadCommandAbstract.class);

        @NotNull final Class[] CLASSES = new Class[classes.size()];
        classes.toArray(CLASSES);

        terminalService.initCommands(CLASSES);
        try {
            start();
        }catch (Exception e){
            terminalService.printlnArbitraryMassage("Something went wrong...");
            e.printStackTrace();
        }
    }

    private void start() throws Exception{
        terminalService.printlnArbitraryMassage("*** WELCOME TO TASK MANAGER! ***");
        String command = "";
        autoLogin();
        while (!"exit".equals(command)){
            String[] commands = terminalService.readLine("\nInsert your command in low case or command <help>:").split(" ");
            try {
                execute(commands);
            }catch (Exception e){ e.printStackTrace();}
            command = commands[0];
        }
    }

    private void execute(@Nullable String... command) throws Exception{
        if (Objects.isNull(command) || Objects.isNull(command[0]))
            return;
        String firstParam = command[0];
        if ( Objects.isNull(command) || firstParam.isEmpty())
            return;

        AbstractCommand abstractCommand = terminalService.getMapCommands().get(firstParam);

        if (Objects.isNull(abstractCommand))
            return;

        abstractCommand.execute(command);
    }

    @NotNull
    @Override
    public ISessionService getSessionService(){return sessionService;};

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITerminalService getTerminalService() {
        return terminalService;
    }


    public void autoLogin() throws Exception_Exception {
        User user = getUserService().getUserByNamePass("admin", "2");
        if ( Objects.nonNull(user) ){
            Session session = user.getSession();
            getSessionService().setSession(session);
            getProjectService().setSession(session);
            getTaskService().setSession(session);
            getUserService().setSession(session);
            getUserService().setAuthorized(true);
            getUserService().setAuthorizedUser(user);
            getTerminalService().printMassageCompleted();
        }
    }
}