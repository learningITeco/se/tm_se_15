package ru.potapov.tm.api;
import ru.potapov.tm.endpoint.*;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;

public interface IService {
    void loadBinar(@Nullable Session session) throws Exception;
    void saveBinar(@Nullable Session session) throws Exception;

    void saveJaxb(@Nullable Session session, final boolean formatXml) throws Exception;
    void loadJaxb(@Nullable Session session, final boolean formatXml) throws Exception;

    void saveFasterXml(@Nullable Session session) throws Exception;
    void loadFasterXml(@Nullable Session session) throws Exception;

    void saveFasterJson(@Nullable Session session) throws Exception;
    void loadFasterJson(@Nullable Session session) throws Exception;
}
